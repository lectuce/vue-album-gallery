import axios from 'axios'

// axios.defaults.baseURL = window.env.API_ROOT
// axios.defaults.baseURL = 'http://localhost:2712/api'
axios.defaults.baseURL = 'https://loannguyen.tuanngo.xyz/api'

export default {
  install: (Vue) => {
    Object.defineProperty(Vue.prototype, '$http', {value: axios})
  }
}
