// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import iView from 'iview'
import VueImg from 'v-img'
import router from './router'
import './config/global-mixin'
import 'iview/dist/styles/iview.css'
import AxiosPlugin from './config/axios'
import locale from 'iview/dist/locale/en-US'

const vueImgConfig = {
  // Use `alt` attribute as gallery slide title
  altAsTitle: true,
  // Display 'download' button near 'close' that opens source image in new tab
  sourceButton: true,
  // Show thumbnails for all groups with more than 1 image
  thumbnails: true
}

Vue.config.productionTip = false

Vue.use(VueImg, vueImgConfig)
Vue.use(AxiosPlugin)
Vue.use(iView, { locale })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
