import Vue from 'vue'
import Router from 'vue-router'
import HappyBirthday from '@/components/HappyBirthday'
import BirthdayGift from '@/components/BirthdayGift'
import ImageGallery from '@/components/ImageGallery'
import LoginPage from '@/components/LoginPage'
import Memories from '@/components/Memories'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'LoginPage',
      component: LoginPage
    },
    {
      path: '/gallery',
      name: 'ImageGallery',
      component: ImageGallery
    },
    {
      path: '/memories',
      name: 'Memories',
      component: Memories
    },
    {
      path: '/happy-birthday',
      name: 'HappyBirthday',
      component: HappyBirthday
    },
    {
      path: '/birthday-gift',
      name: 'BirthdayGift',
      component: BirthdayGift
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

export default router
