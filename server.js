const path = require('path')
const express = require('express')
const fileParser = require('connect-multiparty')()
const cloudinary = require('cloudinary')
const bodyParser = require('body-parser')
const serveStatic = require('serve-static')
const router = express.Router()

cloudinary.config({
  cloud_name: 'nat2710',
  api_key: 898537316183562,
  api_secret: 'aItK0xehMZ0ECZofkUWnJq_oATA'
})

// Define port
const serverPort = process.env.PORT || 2712

// Start the application after the database connection is ready
const app = express()

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
  next()
})

app.use(bodyParser.json()) // Support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // Support URL-encoded bodies
  extended: true
}))

app.use(serveStatic(path.join(__dirname, '/dist')))
  .listen(serverPort, () => console.log(`Listening on ${serverPort}`))

app.use('/api', router)

/**
 * Get all images
 */
router.route('/my-album')
  .get((req, res) => {
    const nextCursor = req.query.nextCursor

    cloudinary.v2.api.resources({
      type: 'upload',
      direction: 'desc',
      context: true,
      max_results: 25,
      next_cursor: nextCursor
    }, (error, result) => {
      if (error) {
        console.log('err')
      } else {
        res.send(result)
      }
    })
  })

/**
 * Upload image
 */
router.route('/upload/image')
  .post(fileParser, (req, res) => {
    const imageFile = req.files.image
    const additionalInfo = {
      caption: req.body.caption
    }

    cloudinary.v2.uploader.upload(imageFile.path, {
      use_filename: true,
      crop: 'limit', width: 1920, height: 1920,
      context: additionalInfo,
    }, (error, result) => {
      if (error) {
        res.sendStatus(500)
      } else {
        res.send({
          status: 200,
          result: result
        })
      }
    })
  })

/**
 * Update image
 */
router.route('/update/image')
  .post((req, res) => {
    const additionalInfo = {}

    if (req.body.alt) {
      additionalInfo.alt = req.body.alt
    }

    if (req.body.caption) {
      additionalInfo.caption = req.body.caption
    }

    cloudinary.v2.api.update(req.body.publicId, {context: additionalInfo}, (error, result) => {
      if (error) {
        res.sendStatus(500)
      } else {
        res.send({
          status: 200,
          result: result
        })
      }
    })
  })

/**
 * Delete image
 */
router.route('/delete/image')
  .delete((req, res) => {
    const publicId = req.query.publicId

    if (publicId) {
      cloudinary.v2.api.delete_resources([publicId], (error, result) => {
        if (error) {
          res.sendStatus(500)
        } else {
          res.send({
            status: 200,
            result: result
          })
        }
      })
    }
  })

/*
  Handle all requests not served from server
 */
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/dist/index.html'))
})
